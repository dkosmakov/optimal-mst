package org.algorithm.mst.decision;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.algorithm.mst.decision.DecisionTree.Comparison.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.list;

class DecisionTreeTest {

    @Test
    void shouldValidateDecisionTree() {
        DecisionTree decisionTree = new DecisionTree(new DecisionTree.Comparison[]{of(0, 1), of(0, 2)});

        List<Integer> edgeWeights = list(2, 1, 0);

        int bucketId = decisionTree.classify(edgeWeights);

        boolean correct = decisionTree.putIfAbsentOrCheck(bucketId, list(1, 2));
        assertThat(correct).isTrue();

        correct = decisionTree.putIfAbsentOrCheck(bucketId, list(1, 2));
        assertThat(correct).isTrue();

        correct = decisionTree.putIfAbsentOrCheck(bucketId, list(0, 1));
        assertThat(correct).isFalse();

        correct = decisionTree.putIfAbsentOrCheck(bucketId, list(1, 2, 3));
        assertThat(correct).isFalse();
    }
}