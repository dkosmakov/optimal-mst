package org.algorithm.mst.decision;

import org.algorithm.mst.graph.edge.WeightedEdge;
import org.algorithm.mst.util.MeldableLinkedList;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.*;

class DecisionTreeMSTTest {

    @Test
    void shouldReturnCorrectMSTForDifferentGraphs() {
        DecisionTreeMST decisionTreeMST = new DecisionTreeMST(4);
        decisionTreeMST.precompute();

        List<WeightedEdge> edges = newArrayList(
                new WeightedEdge(0, 1, 20L),
                new WeightedEdge(0, 2, 30L),
                new WeightedEdge(1, 2, 40L));
        MeldableLinkedList<WeightedEdge> mst = decisionTreeMST.findMST(3, edges);
        assertThat(mst).containsOnly(new WeightedEdge(0, 1, 20L), new WeightedEdge(0, 2, 30L));

        edges = newArrayList(
                new WeightedEdge(0, 1, 20L),
                new WeightedEdge(0, 2, 30L),
                new WeightedEdge(1, 2, 10L));

        mst = decisionTreeMST.findMST(3, edges);
        assertThat(mst).containsOnly(new WeightedEdge(0, 1, 20L), new WeightedEdge(1, 2, 10L));
    }
}