package org.algorithm.mst;


import org.algorithm.mst.util.MeldableLinkedList;
import org.junit.jupiter.api.Test;

class MeldableLinkedListTest {

    @Test
    void test() {
        MeldableLinkedList<Integer> integerMeldableLinkedList = new MeldableLinkedList<>();

        integerMeldableLinkedList.add(1);
        integerMeldableLinkedList.add(2);
        integerMeldableLinkedList.add(3);
        integerMeldableLinkedList.add(4);
        integerMeldableLinkedList.add(5);

        MeldableLinkedList<Integer> newList = new MeldableLinkedList<>(3);
        newList.add(44);
        newList.add(45);
        newList.add(46);
        newList.add(47);

        integerMeldableLinkedList.meld(newList);
        System.out.println(integerMeldableLinkedList.remove());
        System.out.println(integerMeldableLinkedList.remove());
        System.out.println(integerMeldableLinkedList.remove());
        System.out.println(integerMeldableLinkedList.remove());
        System.out.println(integerMeldableLinkedList.remove());
        System.out.println(integerMeldableLinkedList.remove());
        System.out.println(integerMeldableLinkedList.remove());
        System.out.println(integerMeldableLinkedList.remove());
        System.out.println(integerMeldableLinkedList.remove());

        integerMeldableLinkedList.meld(new MeldableLinkedList<>(1000));
        System.out.println(integerMeldableLinkedList.remove());
        System.out.println(integerMeldableLinkedList.remove());

        System.out.println();
    }
}