package org.algorithm.mst;

import org.algorithm.mst.util.heap.SoftHeap;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.IntStream;

class SoftHeapTest {

    @Test
    public void test() {
        Random r = new Random();
        int elemsCount = 1000000;
        long[] elems = new long[elemsCount];
        IntStream.range(0, elemsCount).forEach(i -> elems[i] = r.nextInt(elemsCount));

        long[] toSort = new long[elemsCount];

        long start = System.currentTimeMillis();
        IntStream.range(0, elemsCount).forEach(i -> toSort[i] = elems[i]);

        Arrays.sort(toSort);
        long end = System.currentTimeMillis();

        System.out.println(end - start + " ms");
        System.out.println(Arrays.toString(Arrays.copyOfRange(toSort, 0, 1000)));


        long start2 = System.currentTimeMillis();
        SoftHeap<Long> integerSoftHeap = SoftHeap.create(Function.identity(), 0.01);
        IntStream.range(0, elemsCount).forEach(i -> integerSoftHeap.insert(elems[i]));

        long[] result = new long[elemsCount];
        IntStream.range(0, elemsCount).forEach(i -> {
            result[i] = integerSoftHeap.extractMin().getValue();
        });

        long end2 = System.currentTimeMillis();
        assert integerSoftHeap.isEmpty();

        System.out.println(end2 - start2 + " ms");
        System.out.println(Arrays.toString(Arrays.copyOfRange(result, 0, 1000)));

        long start3 = System.currentTimeMillis();

        PriorityQueue<Long> priorityQueue = new PriorityQueue<>();
        IntStream.range(0, elemsCount).forEach(i -> priorityQueue.add(elems[i]));

        long[] result2 = new long[elemsCount];
        IntStream.range(0, elemsCount).forEach(i -> {
            result2[i] = priorityQueue.remove();
        });

        long end3 = System.currentTimeMillis();

        System.out.println(end3 - start3 + " ms");
        System.out.println(Arrays.toString(Arrays.copyOfRange(result2, 0, 1000)));

    }

    @Test
    void shouldNotReturnCorruptedOnSmallErrorRate() {
        Random r = new Random();
        int elemsCount = 1000000;
        long[] elems = new long[elemsCount];
        IntStream.range(0, elemsCount).forEach(i -> elems[i] = r.nextInt(elemsCount));

        SoftHeap<Long> integerSoftHeap = SoftHeap.create(Function.identity(), 0.000001);
        IntStream.range(0, elemsCount).forEach(i -> integerSoftHeap.insert(elems[i]));

        IntStream.range(0, elemsCount).forEach(i -> {
            assert !integerSoftHeap.extractMin().isCorrupted();
        });

    }
}