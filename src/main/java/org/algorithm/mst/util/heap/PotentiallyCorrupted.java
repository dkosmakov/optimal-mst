package org.algorithm.mst.util.heap;

import lombok.Value;

@Value
public class PotentiallyCorrupted<T> {

    T value;
    boolean corrupted;

    public static <T> PotentiallyCorrupted<T> corrupted(T value) {
        return new PotentiallyCorrupted<>(value, true);
    }

    public static <T> PotentiallyCorrupted<T> notCorrupted(T value) {
        return new PotentiallyCorrupted<>(value, false);
    }

}
