package org.algorithm.mst.util.heap;

import org.algorithm.mst.util.MeldableLinkedList;

import java.util.function.Function;

import static org.algorithm.mst.util.heap.PotentiallyCorrupted.corrupted;
import static org.algorithm.mst.util.heap.PotentiallyCorrupted.notCorrupted;

public class SoftHeap<T> {

    final Function<T, Long> makeKey;

    private BinaryTree first;
    private int rank;
    private int r;

    public static <T> SoftHeap<T> create(Function<T, Long> makeKey, double errorRate) {
        int r = r(errorRate);
        return new SoftHeap<>(makeKey, r);
    }

    public static <T> SoftHeap<T> create(Function<T, Long> makeKey, double errorRate, T elem) {
        int r = r(errorRate);
        return new SoftHeap<>(makeKey, r, elem);
    }

    private SoftHeap(Function<T, Long> makeKey, int r) {
        this.makeKey = makeKey;
        this.r = r;
    }

    private SoftHeap(Function<T, Long> makeKey, int r, T elem) {
        this.makeKey = makeKey;
        this.first = new BinaryTree(elem);
        this.r = r;
    }

    public BinaryTree head() {
        return first;
    }

    public void insert(T elem) {
        meld(new SoftHeap<>(this.makeKey, r, elem));
    }

    public PotentiallyCorrupted<T> extractMin() {
        if (first == null) return null;
        BinaryTree sufminTree = first.sufmin;
        Node root = sufminTree.root;
        T elem = root.list.remove();
        PotentiallyCorrupted<T> result = root.ckey == makeKey.apply(elem) ? notCorrupted(elem) : corrupted(elem);
        if (root.list.size() <= root.size / 2) {
            if (!root.isLeaf()) {
                root.sift();
                updateSufMin(sufminTree);
            } else if (root.list.isEmpty()) {
                removeTree(sufminTree);
            }
        }
        return result;
    }

    public boolean isEmpty() {
        return first == null;
    }

    // Returns new melded heap
    private void meld(SoftHeap<T> second) {
        if (this.first == null) {
            this.first = second.first;
            return;
        }
        SoftHeap<T> smaller = this;
        SoftHeap<T> larger = second;

        if (smaller.rank > larger.rank) {
            SoftHeap<T> temp = smaller;
            smaller = larger;
            larger = temp;
        }

        rank = larger.rank;

        // merge heap with smaller rank to the heap with larger rank
        mergeTrees(smaller.first, larger.first);
        repeatedCombine(smaller.rank);
    }

    private void repeatedCombine(int rankUpTo) {
        BinaryTree current = this.first;
        while (current.next != null) {
            if (current.rank() == current.next.rank()) {
                if (current.next.next == null || current.rank() != current.next.next.rank()) {
                    current.root = combine(current.root, current.next.root);
                    this.rank = Math.max(rank, current.rank());
                    removeTree(current.next);
                    continue;
                }
            } else if (current.rank() > rankUpTo) {
                break;
            }
            current = current.next;
        }

        updateSufMin(current);
    }

    private void updateSufMin(BinaryTree current) {
        while (current != null) {
            if (current.next == null || current.root.ckey < current.next.sufmin.root.ckey) {
                current.sufmin = current;
            } else {
                current.sufmin = current.next.sufmin;
            }
            current = current.prev;
        }
    }

    private void removeTree(BinaryTree tree) {
        if (tree.prev == null) {
            first = tree.next;
        } else {
            tree.prev.next = tree.next;
            updateSufMin(tree.prev);
        }
        if (tree.next != null) {
            tree.next.prev = tree.prev;
        }
    }

    private Node combine(Node firstNode, Node secondNode) {
        Node newRoot = new Node();
        newRoot.left = firstNode;
        newRoot.right = secondNode;
        newRoot.rank = firstNode.rank + 1;
        if (newRoot.rank <= r) {
            newRoot.size = 1;
        } else {
            newRoot.size = (3 * firstNode.size + 1) / 2;
        }
        newRoot.sift();
        return newRoot;
    }

    private void mergeTrees(BinaryTree treeFromSmallerRank, BinaryTree treeFromHigherRank) {
        while (treeFromSmallerRank != null) {
            while (treeFromSmallerRank.rank() > treeFromHigherRank.rank()) {
                treeFromHigherRank = treeFromHigherRank.next;
            }
            BinaryTree nextSourceTree = treeFromSmallerRank.next;
            insertTree(treeFromSmallerRank, treeFromHigherRank);
            treeFromSmallerRank = nextSourceTree;
        }
    }

    private void insertTree(BinaryTree prevTree, BinaryTree targetTree) {
        prevTree.next = targetTree;
        if (targetTree.prev == null) {
            this.first = prevTree;
        } else {
            targetTree.prev.next = prevTree;
            prevTree.prev = targetTree.prev;
        }
        targetTree.prev = prevTree;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SoftHeap of rank ").append(rank).append(" with ").append(" elements\n");
        for (BinaryTree heap = first; heap != null; heap = heap.next) {
            sb.append(heap.prev).append(" - ").append(heap).append(" - ").append(heap.next).append("\n");
//            sb.append("Suffix-Min points to ").append(heap.sufMin).append("\n");
        }
        return sb.toString();
    }

    public class BinaryTree {

        public Node root;
        public BinaryTree next;
        private BinaryTree prev;
        private BinaryTree sufmin;

        public int rank() {
            return root.rank;
        }

        BinaryTree(T elem) {
            root = new Node(elem);
            sufmin = this;
        }

        @Override
        public String toString() {
            return "BinaryTree{" +
                    "root=" + root +
                    ", rank=" + rank +
                    ", suffmin=" + (sufmin == this ? "this" : sufmin) +
                    '}';
        }
    }

    public class Node {

        int rank = 0;
        int size = 1;
        long ckey;
        public MeldableLinkedList<T> list;
        public Node left;
        public Node right;

        Node(T elem) {
            list = new MeldableLinkedList<>();
            list.add(elem);
            ckey = makeKey.apply(elem);
        }

        Node() {
            list = new MeldableLinkedList<>();
        }

        void sift() {
            while (list != null && list.size() < size && !isLeaf()) {
                if (left == null || (right != null && left.ckey > right.ckey)) {
                    swapLeafs();
                }
                list.meld(left.list); // Should be O(1) but java doesn't have such list)
                ckey = left.ckey;
                left.list = new MeldableLinkedList<>();
                if (left.isLeaf()) {
                    left = null;
                } else {
                    left.sift();
                }
            }
        }

        private void swapLeafs() {
            Node temp = left;
            left = right;
            right = temp;
        }

        boolean isLeaf() {
            return left == null && right == null;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "rank=" + rank +
                    ", size=" + size +
                    ", ckey=" + ckey +
                    ", list=" + list +
                    ", left=" + left +
                    ", right=" + right +
                    '}';
        }

    }

    public static int r(double errorRate) {
        double v = 1 / errorRate;
        return log2(v) + 5;
    }

    public static int log2(double x) {
        return (int) (Math.ceil(Math.log(x) / Math.log(2)));
    }

}
