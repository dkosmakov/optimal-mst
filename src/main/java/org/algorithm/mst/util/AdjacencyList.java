package org.algorithm.mst.util;


import org.algorithm.mst.graph.Graph;
import org.algorithm.mst.graph.edge.DirectedEdge;

import java.util.ArrayList;

/**
 * 
 * Represents the AdjacencyList of a Graph.
 * @param <T> the edge type of the graph
 */
public final class AdjacencyList<T extends DirectedEdge> extends ArrayList<MeldableLinkedList<T>> {

    private static final long serialVersionUID = 1L;

    /**
     * Creates a new AdjacenyList for a graph with count vertices
     * @param count the number of vertices in the AdjacencyList
     */
    public AdjacencyList(int count) {
        super(count);
        for (int i = 0; i < count; i++)
            add(new MeldableLinkedList<>());
    }

    /**
     * Takes a {@link Graph} graph and transforms it into an AdjacencyList
     * @param <T> the edge type of the edges in the graph
     * @param g the {@link Graph} to be transformed
     * @return the AdjacencyList for the given Graph
     */
    public static <T extends DirectedEdge<T>> AdjacencyList<T> of(Graph<T> g) {
        return of(g.vertices, g.edges);
    }

    /**
     * 
     * Takes a graph given by the number of vertices and an {@link Iterable} of edges and transforms it into an AdjacencyList
     * @param <T> the edge type of the edges in the graph
     * @param vertices the number of vertices
     * @param edges an {@link Iterable} of edges
     * @return the AdjacencyList for the given Graph
     */
    public static <T extends DirectedEdge<T>> AdjacencyList<T> of(int vertices, Iterable<? extends T> edges) {
        AdjacencyList<T> adjacency = new AdjacencyList<>(vertices);
        for (T e : edges) {
            adjacency.append(e.from(), e);
            adjacency.append(e.to(), e.reversed());
        }
        return adjacency;
    }
    
    /**
     * Returns an {@link MeldableLinkedList} of edges for a given vertex.
     * @param vertex specifies the vertex
     * @return returns an {@link MeldableLinkedList} of edges for the given vertex
     */
    public MeldableLinkedList<T> get(int vertex) {
        return super.get(vertex);
    }

    /**
     * Appends an edge to the adjacency of a given vertex.
     * @param vertex specifies the vertex
     * @param edge the edge to be appended
     */
    public void append(int vertex, T edge) {
        get(vertex).add(edge);
    }

    @Override
    public String toString() {
        if (size() == 0)
            return "-";
        StringBuilder sb = new StringBuilder();
        for (int v = 0; v < size(); v++)
            sb.append(v).append(": ").append(get(v)).append("\n");
        return sb.toString();
    }
}
