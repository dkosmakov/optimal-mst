package org.algorithm.mst.util.unionfind;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class WeightedUnionFind {

    private int[] uf;
    private int[] weights;

    private int componentsCount;

    public WeightedUnionFind(int size) {
        this.uf = new int[size];
        this.weights = new int[size];
        IntStream.range(0, size).forEach(i -> this.uf[i] = i);
        IntStream.range(0, size).forEach(i -> this.weights[i] = 1);
        this.componentsCount = size;
    }

    public void connect(int i, int j) {
        int rootI = root(i);
        int rootJ = root(j);
        if (rootI != rootJ) {
            if (weights[rootI] > weights[rootJ]) {
                uf[rootJ] = rootI;
                weights[rootI] += weights[rootJ];
            } else {
                uf[rootI] = rootJ;
                weights[rootJ] += weights[rootI];
            }
            componentsCount--;
        }
    }

    public int getComponentsCount() {
        return componentsCount;
    }

    public boolean connected(int i, int j) {
        return root(i) == root(j);
    }

    public int root(int i) {
        while (uf[i] != i) {
            uf[i] = uf[uf[i]];
            i = uf[i];
        }
        return i;
    }

    //Чисто шоб проверить
    public List<List<Integer>> getComponents() {
        Map<Integer, List<Integer>> components = new HashMap<>();
        for (int i = 0; i < uf.length; i++) {
            if (uf[i] == i) {
                components.put(i, new ArrayList<>());
            }
        }

        for (int i = 0; i < uf.length; i++) {
            components.get(root(i)).add(i);
        }

        return new ArrayList<>(components.values());
    }
}