package org.algorithm.mst.util;


import org.algorithm.mst.graph.Graph;
import org.algorithm.mst.graph.edge.ContractedEdge;
import org.algorithm.mst.graph.edge.DirectedEdge;

import java.util.*;
import java.util.stream.Collectors;

public class GraphUtils {

    public static <T extends DirectedEdge<T>> Graph<ContractedEdge<T>> contractIntoGraph(int vertices, MeldableLinkedList span, MeldableLinkedList edges) {

        // find connected components
        List<List<Integer>> componentVertecies = components(vertices, span);
        // obtain component mapping
        int[] component = componentMapping(vertices, componentVertecies);
        // find number of components
        int componentCount = componentVertecies.size();

        // contract components
        MeldableLinkedList<ContractedEdge<T>> contracted = new MeldableLinkedList<>();
        for (T e : (MeldableLinkedList<T>) edges) {
            if (component[e.from()] == component[e.to()])
                continue;
            contracted.add(new ContractedEdge<>(component[e.from()], component[e.to()], e));
        }
        // remove duplicates
        MeldableLinkedList<ContractedEdge<T>> remainingEdges = removeDuplicates(componentCount, contracted);

        return new Graph<>(componentCount, remainingEdges);
    }

    public static <T extends DirectedEdge<T>> MeldableLinkedList<ContractedEdge<T>> contract(int vertices, List<List<Integer>> components, MeldableLinkedList<ContractedEdge<T>> edges) {

        // obtain component mapping
        int[] component = componentMapping(vertices, components);
        // find number of components
        int componentCount = components.size();

        // contract components
        MeldableLinkedList<ContractedEdge<T>> contracted = new MeldableLinkedList<>();
        for (ContractedEdge<T> e : edges) {
            if (component[e.from()] == component[e.to()])
                continue;
            contracted.add(new ContractedEdge(component[e.from()], component[e.to()], e));
        }

        // remove duplicates
        return removeDuplicates(componentCount, contracted);
    }

    public static <T extends DirectedEdge<T>> List<List<Integer>> components(int vertices, Iterable<T> edges) {

        // generate adjacency list
        AdjacencyList adjacency = AdjacencyList.of(vertices, edges);

        ArrayList<List<Integer>> components = new ArrayList<>();
        boolean[] visited = new boolean[vertices];

        ArrayDeque<Integer> stack = new ArrayDeque<>();
        for (int v = 0; v < vertices; v++) {
            // if we discover a new component ...
            if (visited[v])
                continue;

            ArrayList<Integer> comp = new ArrayList<>();
            // ... perform dfs on component
            stack.push(v);
            while (!stack.isEmpty()) {
                int neighbor = stack.pop();
                // set component id for every connected vertex
                comp.add(neighbor);
                visited[neighbor] = true;
                for (ContractedEdge<T> e : (MeldableLinkedList<ContractedEdge<T>>) adjacency.get(neighbor))
                    if (!visited[e.to()])
                        stack.push(e.to());
            }
            components.add(comp);
        }
        return components;
    }

    public static int[] componentMapping(int vertices, List<List<Integer>> components) {

        int componentMapping[] = new int[vertices];

        for (int c = 0; c < components.size(); c++)
            for (int vertex : components.get(c))
                componentMapping[vertex] = c;

        return componentMapping;
    }

    public static <T extends DirectedEdge> MeldableLinkedList<T> removeDuplicates(int vertices, Iterable<T> edges) {

        MeldableLinkedList<T> sorted = sortEdges(vertices, edges);
        MeldableLinkedList<T> result = new MeldableLinkedList<>();

        T lightest = null;
        // find lightest edge between all pairs of vertices
        for (T e : sorted) {
            // skip self-loops
            if (e.from() == e.to())
                continue;
            // first iteration: initialize candidate for lightest edge
            if (lightest == null)
                lightest = e;
            // different endpoint: save current best
            if (lightest.from() != e.from() || lightest.to() != e.to()) {
                result.add(lightest);
                lightest = e;
            }
            // update lightest if we find a better candidate
            if (lightest.weight() > e.weight())
                lightest = e;
        }
        // append last result
        if (lightest != null)
            result.add(lightest);

        return result;
    }

    public static <T extends DirectedEdge<T>> MeldableLinkedList<T> sortEdges(int vertices, Iterable<T> edges) {
        AdjacencyList<T> firstPass = new AdjacencyList<>(vertices);
        AdjacencyList<T> secondPass = new AdjacencyList<>(vertices);

        // first pass: bucket by e.to
        for (T e : edges) {
            if (e.from() > e.to())
                e = e.reversed();
            firstPass.append(e.to(), e);
        }

        // second pass: bucket by e.from
        for (int v = 0; v < vertices; v++)
            for (T e : firstPass.get(vertices - v - 1))
                secondPass.append(e.from(), e);

        MeldableLinkedList<T> result = new MeldableLinkedList<>();
        for (int v = 0; v < vertices; v++)
            result.meld(secondPass.get(v));
        return result;
    }

    /**
     * Takes an {@link Iterable} of edges and renames the vertices, so that all vertex indices
     * are between 0 and n - 1 where n is the number of vertices
     *
     * @param <T>   the edge type of the edges of the graph
     * @param edges the {@link Iterable} whose vertices shall be renamed
     * @return a {@link Graph} representing the graph but with renamed edges
     */
    public static <T extends DirectedEdge<T>> Graph<ContractedEdge<T>> renameVertices(Iterable<ContractedEdge<T>> edges) {
        Map<Integer, Integer> renamedVertices = new HashMap<>();
        MeldableLinkedList<ContractedEdge<T>> renamedEdges = new MeldableLinkedList<>();
        int vertex = 0;
        for (ContractedEdge<T> edge : edges) {
            int from = edge.from(), to = edge.to();
            if (!renamedVertices.containsKey(from)) {
                renamedVertices.put(from, vertex);
                vertex++;
            }
            if (!renamedVertices.containsKey(to)) {
                renamedVertices.put(to, vertex);
                vertex++;
            }
            renamedEdges.add(new ContractedEdge(renamedVertices.get(from), renamedVertices.get(to), edge));
        }
        return new Graph<>(vertex, renamedEdges);
    }

    public static <T extends DirectedEdge<T>> MeldableLinkedList<ContractedEdge<T>> lightestEdgePerVertex(int vertices, Iterable<ContractedEdge<T>> edges) {

        ArrayList<ContractedEdge<T>> lightest = new ArrayList<>(vertices);
        for (int i = 0; i < vertices; i++)
            lightest.add(null);


        // store lightest edge for each vertex
        for (ContractedEdge<T> ce : edges) {
            if (lightest.get(ce.from()) == null || compare(lightest.get(ce.from()), ce) > 0)
                lightest.set(ce.from(), ce);
            if (lightest.get(ce.to()) == null || compare(lightest.get(ce.to()), ce) > 0)
                lightest.set(ce.to(), ce);
        }

        // remove nulls and duplicates
        MeldableLinkedList<ContractedEdge<T>> result = new MeldableLinkedList<>();
        Set<ContractedEdge<T>> uniqueEdges = lightest.stream().filter(Objects::nonNull).collect(Collectors.toCollection(HashSet::new));
        uniqueEdges.forEach(result::add);
        return result;
    }

    public static <T extends DirectedEdge<T>> MeldableLinkedList<ContractedEdge<T>> flatten(MeldableLinkedList<ContractedEdge<T>> list) {
        MeldableLinkedList<ContractedEdge<T>> flat = new MeldableLinkedList<>();
        list.forEach(e -> flat.add(new ContractedEdge<>(e.from(), e.to(), ((ContractedEdge<T>)e.original).original)));
        return flat;
    }

    public static <T extends DirectedEdge<T>> MeldableLinkedList<ContractedEdge<T>> extractOriginal(MeldableLinkedList<ContractedEdge<T>> list) {
        MeldableLinkedList<ContractedEdge<T>> flat = new MeldableLinkedList<>();
        list.forEach(e -> flat.add(((ContractedEdge<T>)e.original)));
        return flat;
    }

    /**
     * Takes a {@link Graph} of at least doubly contracted edges and removes the second-outermost layer of contracted edges
     * @param <T> the weight type of the edges of the graph
     * @param graph the {@link Graph} of at least doubly contracted edges
     * @return the flattened {@link Graph}
     */
    public static <T extends DirectedEdge<T>> Graph<ContractedEdge<T>> flatten(Graph<ContractedEdge<T>> graph) {
        return new Graph<>(graph.vertices, flatten(graph.edges));
    }

    public static <T extends DirectedEdge<T>> long compare(DirectedEdge<T> first, DirectedEdge<T> second) {
        long byWeight = first.weight() - second.weight();
        if (byWeight != 0)
            return byWeight;
        int byFrom = Integer.compare(first.from(), second.from());
        if (byFrom != 0)
            return byFrom;
        return Integer.compare(first.from(), second.from());
    }
}
