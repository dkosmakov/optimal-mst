package org.algorithm.mst.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Linked list with O(1) meld operation. There is no such class in java library.
 * @param <T>
 */
public class MeldableLinkedList<T> implements Iterable<T> {

    private int size;
    private Node first;
    private Node last;

    public MeldableLinkedList() {
    }

    public MeldableLinkedList(T elem) {
        Node first = new Node(elem);
        this.size = 1;
        this.first = first;
        this.last = first;
    }

    public MeldableLinkedList(Iterable<? extends T> other) {
        other.forEach(this::add);
    }

    public void meld(MeldableLinkedList<T> other) {
        if (other == null || other.first == null) {
            return;
        }
        if (this.last != null) {
            this.last.next = other.first;
            this.last = other.last;
        } else {
            this.first = other.first;
        }

        first = size > 0 ? first : other.first;
        last = other.size > 0 ? other.last : last;

        size += other.size;
        other.clear();
    }

    public void clear() {
        first = last = null;
        size = 0;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public T remove() {
        T elem = first.value;
        first = first.next;
        size -= 1;
        return elem;
    }

    public void add(T elem) {
        this.meld(new MeldableLinkedList<>(elem));
    }

    @Override
    public Iterator<T> iterator() {
        return new Itr(this);
    }

    private class Itr implements Iterator<T> {

        private Node current;

        Itr(MeldableLinkedList<T> list) {
            current = list.first;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            if (current == null)
                throw new NoSuchElementException("No more elements.");
            T e = current.value;
            current = current.next;
            return e;
        }
    }

    public List<T> toList() {
        List<T> list = new ArrayList<>();
        this.iterator().forEachRemaining(list::add);
        return list;
    }
    private class Node {

        private Node next;
        private T value;

        Node(T elem) {
            this.value = elem;
        }
    }

}
