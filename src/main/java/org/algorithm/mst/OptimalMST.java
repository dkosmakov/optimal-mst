package org.algorithm.mst;

import org.algorithm.mst.decision.DecisionTreeMST;
import org.algorithm.mst.graph.Graph;
import org.algorithm.mst.graph.edge.ContractedEdge;
import org.algorithm.mst.graph.edge.DirectedEdge;
import org.algorithm.mst.util.AdjacencyList;
import org.algorithm.mst.util.GraphUtils;
import org.algorithm.mst.util.MeldableLinkedList;
import org.algorithm.mst.util.heap.SoftHeap;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public final class OptimalMST {

    public static <T extends DirectedEdge<T>> MeldableLinkedList<T> compute(int vertices, List<ContractedEdge<T>> edges) {

        MeldableLinkedList<ContractedEdge<T>> wrapper = new MeldableLinkedList<>(edges);

        int maxsize = maxPartitionSize(vertices);
        DecisionTreeMST decisionTrees = new DecisionTreeMST(maxsize);
        decisionTrees.precompute();

        return recurse(vertices, wrapper, decisionTrees);
    }

    private static <T extends DirectedEdge<T>> MeldableLinkedList<T> recurse(int vertices, MeldableLinkedList<ContractedEdge<T>> edges, DecisionTreeMST decisionTrees) {

        if (edges.size() == 0)
            return new MeldableLinkedList<>();

        int maxsize = maxPartitionSize(vertices);

        // Calculate the partitions
        AdjacencyList adjacencyList = AdjacencyList.of(vertices, edges);
        PartitionWrapper<ContractedEdge<T>> partitions = partition(adjacencyList, maxsize, 0.125);

        MeldableLinkedList<ContractedEdge<T>> partitionMSFWithRenamedEdges = new MeldableLinkedList<>();

        // Calculate all MSFs for these subgraphs of fixed size using optimal decision trees
        for (Graph<ContractedEdge<T>> partition : partitions.subGraphs) {
            List<ContractedEdge<T>> contractedEdges = partition.edges.toList();
            MeldableLinkedList<ContractedEdge<T>> mst = decisionTrees.findMST(partition.vertices, contractedEdges);
            partitionMSFWithRenamedEdges.meld(mst);
        }

        MeldableLinkedList<ContractedEdge<T>> partitionMSF = new MeldableLinkedList<>();
        partitionMSFWithRenamedEdges.forEach(e -> partitionMSF.add(new ContractedEdge<>(e.original)));

        // Contract all partitions and calculate the MSF of the contracted graph
        // with Fredman and Tarjan's algorithm in O(m) time
        Graph<ContractedEdge<T>> contractedPartitions = GraphUtils.contractIntoGraph(vertices, partitionMSF, edges);

        // Remove corrupted Edges
        MeldableLinkedList<ContractedEdge<T>> denseCaseEdges = new MeldableLinkedList<>();
        for (ContractedEdge<T> e : contractedPartitions.edges) {
            if (!partitions.corruptedEdges.contains(e.original)) {
                denseCaseEdges.add(e);
            }
        }

        MeldableLinkedList<ContractedEdge<T>> denseCaseMST = FredmanTarjanMST.compute(contractedPartitions.vertices, AdjacencyList.of(contractedPartitions.vertices, denseCaseEdges));

        MeldableLinkedList<ContractedEdge<T>> reducedEdges = new MeldableLinkedList<>();
        denseCaseMST.forEach(e -> reducedEdges.add((ContractedEdge<T>) e.original));
        partitions.corruptedEdges.forEach(reducedEdges::add);
        partitionMSF.forEach(e -> reducedEdges.add((ContractedEdge<T>) e.original));
        // Two Steps of Boruvka's algorithm
        MeldableLinkedList<T> boruvkaEdges = new MeldableLinkedList<>();
        MeldableLinkedList<ContractedEdge<T>> forestEdges;
        Graph<ContractedEdge<T>> contractTwice = new Graph<>(vertices, reducedEdges);

        for (int boruvkaIterations = 0; boruvkaIterations < 2; boruvkaIterations++) {
            forestEdges = GraphUtils.lightestEdgePerVertex(contractTwice.vertices, contractTwice.edges);
            Graph<ContractedEdge<T>> contractedEdgeGraph = GraphUtils.contractIntoGraph(contractTwice.vertices, forestEdges, contractTwice.edges);
            contractTwice = GraphUtils.flatten(contractedEdgeGraph);

            // extract original edges
            forestEdges.iterator().forEachRemaining(e -> boruvkaEdges.add(e.original));
        }
        MeldableLinkedList<T> mst = recurse(contractTwice.vertices, contractTwice.edges, decisionTrees);
        mst.meld(boruvkaEdges);
        return mst;

    }

    private static <T extends DirectedEdge<T>> PartitionWrapper<ContractedEdge<T>> partition(AdjacencyList<ContractedEdge<T>> edges, int maxsize, double errorRate) {

        boolean[] dead = new boolean[edges.size()];
        for (int i = 0; i < dead.length; ++i) {
            dead[i] = false;
        }

        Set<ContractedEdge<T>> corruptedEdges = new HashSet<>();
        List<Graph<ContractedEdge<T>>> partitions = new ArrayList<>();

        // For each vertex find a partition that they are part of
        for (int current = 0; current < edges.size(); ++current) {
            if (dead[current])
                continue;

            dead[current] = true;
            SoftHeap<ContractedEdge<T>> softHeap = SoftHeap.create(DirectedEdge::weight, errorRate);
//            PriorityQueue<ContractedEdge<T>> softHeap = new PriorityQueue<>(Comparator.comparingInt(DirectedEdge::weight));
            AtomicInteger counterAll = new AtomicInteger();
            edges.get(current).forEach(elem -> {
                    counterAll.getAndIncrement();
                    softHeap.insert(elem);
            });

            Set<Integer> currentPartition = new HashSet<>();
            MeldableLinkedList<ContractedEdge<T>> partitionEdges = new MeldableLinkedList<>();
            currentPartition.add(current);
            // Grow the current partition as long as it is smaller than
            // max size and doesn't contain a dead (visited) vertex
            while (currentPartition.size() < maxsize) {
                ContractedEdge<T> minEdge = softHeap.extractMin().getValue();
                // Extract the minimum Edge leading to a Vertex
                // which is not part of the current partition
                while (currentPartition.contains(minEdge.to())) {
                    // In case the edge doesn't lead to a new vertex
                    // it is part of the subgraph induced by the
                    // current partition
                    partitionEdges.add(minEdge);
                    minEdge = softHeap.extractMin().getValue();
                }
                currentPartition.add(minEdge.to());
                partitionEdges.add(minEdge);
                if (dead[minEdge.to()]) {
                    break;
                }
                for (ContractedEdge<T> edge : edges.get(minEdge.to())) {
                    if (!currentPartition.contains(edge.to())) {
                        counterAll.getAndIncrement();
                        softHeap.insert(edge);
                    }
                }
                dead[minEdge.to()] = true;
            }
            // Append the remaining edges with exactly one endpoint in
            // the current partition to the list of corrupted edges
            // and add all of the other edges to the list of edges that
            // are part of the subgraph induced by the current partition
            AtomicInteger corruptedCounter = new AtomicInteger();
            dismantle(softHeap, currentPartition, partitionEdges, corruptedEdges, corruptedCounter);
//            System.out.println(corruptedCounter + "/" + counterAll);
            // Add the subgraph to our list of subgraphs
            partitions.add(GraphUtils.renameVertices(partitionEdges));
        }
        return new PartitionWrapper<>(partitions, corruptedEdges);
    }

    public static String getCurrentLocalDateTimeStamp() {
        return LocalDateTime.now()
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
    }

    public static <T extends DirectedEdge<T>> void dismantle(SoftHeap<ContractedEdge<T>> heap, Set<Integer> currentPartition, MeldableLinkedList<ContractedEdge<T>> partitionEdges, Set<ContractedEdge<T>> corruptedEdges, AtomicInteger corruptedCounter) {
        SoftHeap.BinaryTree current = heap.head();
        AtomicInteger traverse = new AtomicInteger();
        while (current != null) {
            LinkedList<SoftHeap.Node> active = new LinkedList<>();
            SoftHeap.Node currentNode = current.root;
            active.add(currentNode);
            while (!active.isEmpty()) {
                SoftHeap.Node c = active.remove();
                if (c.left != null) {
                    active.add(c.left);
                }
                if (c.right != null) {
                    active.add(c.right);
                }
                c.list.forEach(elem -> {
                    traverse.getAndIncrement();
                    ContractedEdge<T> edge = (ContractedEdge<T>) elem;
                    if (!currentPartition.contains(edge.to())) {
                        if (c.list.size() > 1) {
                            corruptedCounter.getAndIncrement();
                            corruptedEdges.add(edge);
                        }
                    } else {
                        partitionEdges.add(edge);
                    }
                });
            }
            current = current.next;
        }
//        System.out.println("Traversal: " + traverse.get());
    }

    private static int maxPartitionSize(int vertices) {
        return Math.max(1, (int) Math.ceil(log2(log2(log2(vertices)))));
    }

    private static double log2(double x) {
        return Math.log(x) / Math.log(2);
    }

    public static final class PartitionWrapper<T extends DirectedEdge> {
        final List<Graph<T>> subGraphs;
        final Set<T> corruptedEdges;

        public PartitionWrapper(List<Graph<T>> subGraphs, Set<T> corruptedEdges) {
            this.subGraphs = subGraphs;
            this.corruptedEdges = corruptedEdges;
        }
    }
}
