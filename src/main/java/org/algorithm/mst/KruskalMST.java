package org.algorithm.mst;

import org.algorithm.mst.graph.edge.ContractedEdge;
import org.algorithm.mst.graph.edge.DirectedEdge;
import org.algorithm.mst.util.MeldableLinkedList;
import org.algorithm.mst.util.unionfind.WeightedUnionFind;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class KruskalMST {

    public static <T extends DirectedEdge<T>> MeldableLinkedList<T> compute(int vertices, Iterable<T> edges) {
        List<T> sorted = new ArrayList<>();
        edges.forEach(sorted::add);
        Collections.sort(sorted);

        WeightedUnionFind uf = new WeightedUnionFind(vertices);
        MeldableLinkedList<T> result = new MeldableLinkedList<>();

        for (T e : sorted) {
            if (uf.connected(e.from(), e.to())) {
                continue;
            }

            result.add(e);
            uf.connect(e.from(), e.to());
        }

        return result;
    }

    public static <T extends DirectedEdge<T>> MeldableLinkedList<ContractedEdge<T>> computeContracted(int vertices, Iterable<ContractedEdge<T>> edges) {
        List<ContractedEdge<T>> sorted = new ArrayList<>();
        edges.forEach(sorted::add);
        Collections.sort(sorted);

        WeightedUnionFind uf = new WeightedUnionFind(vertices);
        MeldableLinkedList<ContractedEdge<T>> result = new MeldableLinkedList<>();

        for (ContractedEdge<T> e : sorted) {
            if (uf.connected(e.from(), e.to())) {
                continue;
            }

            result.add(e);
            uf.connect(e.from(), e.to());
        }

        return result;
    }

}
