package org.algorithm.mst.decision;

import lombok.RequiredArgsConstructor;
import org.algorithm.mst.graph.edge.DirectedEdge;
import org.algorithm.mst.util.GraphUtils;
import org.algorithm.mst.util.MeldableLinkedList;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DecisionTreeCache {

    private final Map<Integer, Map<GraphStructure, DecisionTree>> cache = new HashMap<>();

    public <T extends DirectedEdge<T>> DecisionTree getDecisionTree(int vertices, List<T> edges) {
        GraphStructure graphStructure = GraphStructure.of(vertices, edges);
        return cache.get(vertices).get(graphStructure);
    }

    public <T extends DirectedEdge<T>> void putDecisionTreeForStructure(int vertices, List<T> edges, DecisionTree decisionTree) {
        GraphStructure graphStructure = GraphStructure.of(vertices, edges);
        cache.putIfAbsent(vertices, new HashMap<>());
        cache.get(vertices).put(graphStructure, decisionTree);
    }

    @RequiredArgsConstructor
    private static class GraphStructure {

        private final int[] froms;
        private final int[] tos;
        private final int hash;

        public static <T extends DirectedEdge<T>> GraphStructure of(int vertices, Iterable<T> edges) {
            MeldableLinkedList<T> sorted = GraphUtils.sortEdges(vertices, edges);
            int[] froms = new int[sorted.size()];
            int[] tos = new int[sorted.size()];
            int index = 0;
            for (T e : sorted) {
                froms[index] = e.from();
                tos[index] = e.to();
                index++;
            }
            int hash = Objects.hash(Arrays.hashCode(froms), Arrays.hashCode(tos));
            return new GraphStructure(froms, tos, hash);
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            GraphStructure that = (GraphStructure) o;
            return Arrays.equals(froms, that.froms) && Arrays.equals(tos, that.tos);
        }

        @Override
        public int hashCode() {
            return hash;
        }
    }

}
