package org.algorithm.mst.decision;

import org.algorithm.mst.KruskalMST;
import org.algorithm.mst.graph.edge.DirectedEdge;
import org.algorithm.mst.graph.edge.IndexedEdge;
import org.algorithm.mst.graph.edge.WeightedEdge;
import org.algorithm.mst.util.MeldableLinkedList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;

import static org.algorithm.mst.decision.Iterators.indexPermutations;

public class DecisionTreeMST {

    private DecisionTreeCache decisionTreeCache;

    private final int maxVerticesCount;

    public DecisionTreeMST(int maxVertices) {
        this.maxVerticesCount = maxVertices;
    }

    public void precompute() {
        decisionTreeCache = new DecisionTreeCache();

        // precomputing decision tree for all vertices count up to maxVerticesCount including
        IntStream.range(2, maxVerticesCount + 1).forEach(verticesCount -> {

            // Generating all possible edges for verticesCount vertices
            List<WeightedEdge> possibleEdges = new ArrayList<>();
            Iterators
                    .ascendingIntPairs(verticesCount, (i, j) -> new WeightedEdge(i, j, 0L))
                    .forEach(possibleEdges::add);

            // iterating through all edge configurations (graph structures)
            for (List<WeightedEdge> edges : Iterators.powerSet(possibleEdges)) {
                if (edges.size() <= 1) {
                    continue;
                }

                // Iterating through all depths
                int maximumDepth = verticesCount * verticesCount;
                for (int depth = 0; depth < maximumDepth; depth++) {

                    // WARN it's not optimal, cause it generates all possible comparisons, but if we compared
                    // for example 0>1 on the first level, we can omit this comparison on next levels.
                    Iterator<DecisionTree> decisionTreesIterator = DecisionTree.enumerateTrees(depth, edges.size()).iterator();
                    boolean correctDecisionTreeFound = false;
                    DecisionTree decisionTree = null;
                    while (decisionTreesIterator.hasNext() && !correctDecisionTreeFound) {
                        decisionTree = decisionTreesIterator.next();
                        Iterator<List<Integer>> permutationsIterator = indexPermutations(edges.size()).iterator();
                        boolean permutationValid = true;
                        while (permutationsIterator.hasNext() && permutationValid) {
                            List<Integer> permutation = permutationsIterator.next();
                            List<IndexedEdge<WeightedEdge>> permutedEdges = new ArrayList<>();
                            // Create graph with permuted edge weights
                            for (int index = 0; index < edges.size(); ++index) {
                                permutedEdges.add(new IndexedEdge<>(index,
                                        edges.get(index).reweighted(permutation.get(index))));
                            }

                            MeldableLinkedList<IndexedEdge<WeightedEdge>> mst = KruskalMST.compute(verticesCount, permutedEdges);

                            List<Integer> edgeIndices = new ArrayList<>();
                            for (IndexedEdge<WeightedEdge> edge : mst) {
                                edgeIndices.add(edge.getIndex());
                            }
                            Collections.sort(edgeIndices);

                            int bucketId = decisionTree.classify(permutedEdges);
                            permutationValid = decisionTree.putIfAbsentOrCheck(bucketId, edgeIndices);
                        }
                        // here we checked all permutations for decision tree and can stop if we found one.
                        // It will be optimal for certain graph structure
                        if (permutationValid) {
                            correctDecisionTreeFound = true;
                        }
                    }

                    if (correctDecisionTreeFound) {
                        System.out.printf("Correct decision tree for edges %s found on the depth '%s'\n", edges, depth);
                        System.out.printf("Decision tree: \n%s", decisionTree);
                        decisionTreeCache.putDecisionTreeForStructure(verticesCount, edges, decisionTree);
                        break;
                    }

                }

            }
        });
    }

    public <T extends DirectedEdge> MeldableLinkedList<T> findMST(int vertices, List<T> edges) {
        validateInput(vertices);
        if (vertices < 2) {
            return new MeldableLinkedList<>();
        }
        if (edges.size() <= 1) {
            return new MeldableLinkedList<>(edges);
        }
        DecisionTree decisionTree = decisionTreeCache.getDecisionTree(vertices, edges);
        return decisionTree.findMST(edges);
    }

    private void validateInput(int vertices) {
        if (decisionTreeCache == null) {
            throw new IllegalStateException("Decision tree cache is not initialized");
        }
        if (vertices > maxVerticesCount) {
            throw new IllegalArgumentException("No precomputed solutions exist for graph size " + vertices + ".");
        }
    }

}
