package org.algorithm.mst.decision;

import lombok.Value;
import org.algorithm.mst.util.MeldableLinkedList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.StreamSupport;

@Value
public class DecisionTree {

    Comparison[] comparisons;
    // contains bucket id to mst edge indices mapping
    // list of indices is always naturally sorted
    Map<Integer, List<Integer>> mstIndices;

    public DecisionTree(Comparison[] comparisons) {
        this.comparisons = comparisons;
        this.mstIndices = new HashMap<>();
    }

    public boolean putIfAbsentOrCheck(int bucketId, List<Integer> correctMst) {
        if (mstIndices.containsKey(bucketId)) {
            List<Integer> currentMstIndices = mstIndices.get(bucketId);
            return sameSize(correctMst, currentMstIndices)
                    && currentMstCorrect(correctMst, currentMstIndices);
        } else {
            mstIndices.put(bucketId, correctMst);
            return true;
        }
    }

    public <T extends Comparable<T>> MeldableLinkedList<T> findMST(List<T> edges) {

        int bucketId = classify(edges);
        List<Integer> mstIndices = this.mstIndices.get(bucketId);
        MeldableLinkedList<T> result = new MeldableLinkedList<>();
        mstIndices.forEach(index -> result.add(edges.get(index)));
        return result;
    }

     // list of integers contain weights for sorted edges,
     // list[0] contains the weight for edge with id 0, etc...
    public <T extends Comparable> int classify(List<T> edgeWeights) {
        int index = 0;
        while (index < comparisons.length) {
            Comparison c = comparisons[index];
            T firstEdgeWeight = edgeWeights.get(c.firstEdgeIndex);
            T secondEdgeWeight = edgeWeights.get(c.secondEdgeIndex);
            // if firstEdgeWeight < secondEdgeWeight
            if (firstEdgeWeight.compareTo(secondEdgeWeight) < 0) {
                // go to left child
                index = 2 * index + 1;
            } else {
                // go to right child
                index = 2 * index + 2;
            }
        }
        return indexToBucket(index);
    }

    public static Iterable<DecisionTree> enumerateTrees(int depth, int edges) {
        if (depth > 29)
            throw new IllegalArgumentException("Unmanageable tree depth.");
        int length = (1 << (depth + 1)) - 1;

        List<Comparison> comparisons = new ArrayList<>();
        Iterators.ascendingIntPairs(edges, Comparison::new).forEach(comparisons::add);

        return () -> StreamSupport.stream(Iterators.combinations(length, comparisons).spliterator(), false)
                .map(l -> new DecisionTree(l.toArray(new Comparison[length])))
                .iterator();
    }

    private int indexToBucket(int index) {
        return index - comparisons.length;
    }

    private boolean currentMstCorrect(List<Integer> correctMst, List<Integer> currentMstIndices) {
        for (int i = 0; i < correctMst.size(); i++) {
            if (!correctMst.get(i).equals(currentMstIndices.get(i))) {
                return false;
            }
        }
        return true;
    }

    private boolean sameSize(List<Integer> correctMst, List<Integer> currentMstIndices) {
        return correctMst.size() == currentMstIndices.size();
    }

    private void appendLevel(final int start, final StringBuilder sb, final int level) {
        for (int j = 0; j < level - 1; j++)
            sb.append("  ");
        if (level > 0)
            sb.append("\u2514 ");

        if (start >= comparisons.length) {
            sb.append("Bucket(").append(indexToBucket(start)).append(")\n");
            return;
        }

        sb.append("Comparison(").append(comparisons[start].firstEdgeIndex)
                .append(" < ").append(comparisons[start].secondEdgeIndex).append(")\n");

        for (int i = 0; i < 2; i++) {
            int index = 2 * start + i + 1;
            appendLevel(index, sb, level + 1);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String mstIndexes = mstIndices.toString();
        sb.append(mstIndexes).append("\n");
        appendLevel(0, sb, 0);
        return sb.toString();
    }

    // represents a comparison between edges
    @Value
    static class Comparison {

        int firstEdgeIndex;
        int secondEdgeIndex;

        static Comparison of(int firstEdgeIndex, int secondEdgeIndex) {
            return new Comparison(firstEdgeIndex, secondEdgeIndex);
        }

    }

}
