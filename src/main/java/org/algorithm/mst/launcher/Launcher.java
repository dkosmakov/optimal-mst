package org.algorithm.mst.launcher;

import org.algorithm.mst.FredmanTarjanMST;
import org.algorithm.mst.KruskalMST;
import org.algorithm.mst.OptimalMST;
import org.algorithm.mst.graph.edge.ContractedEdge;
import org.algorithm.mst.graph.edge.DirectedEdge;
import org.algorithm.mst.graph.edge.WeightedEdge;
import org.algorithm.mst.util.AdjacencyList;
import org.algorithm.mst.util.MeldableLinkedList;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Launcher {

    public static void main(String[] args) throws IOException {

        GraphGenerator.main(args);
        List<WeightedEdge> weightedEdges = new ArrayList<>();
        int verticesCount;

        try (BufferedReader br = new BufferedReader(new FileReader("graph.txt"))) {
            String line = br.readLine();
            verticesCount = Integer.parseInt(line);
            while ((line = br.readLine()) != null && !line.equals("")) {
                String[] words = line.split(" ");
                int from = Integer.parseInt(words[0]);
                int to = Integer.parseInt(words[1]);
                long weight = Long.parseLong(words[2]);

                weightedEdges.add(new WeightedEdge(from, to, weight));
            }
        } catch (IOException e) {
            System.err.println("A fatal error occurred" + e);
            return;
        }

        Collections.shuffle(weightedEdges);
        List<ContractedEdge<WeightedEdge>> contractedEdges = weightedEdges.stream().map(ContractedEdge::new).collect(Collectors.toList());
        long now;
        for (int i = 0; i < 1; i++) {
            now = System.currentTimeMillis();
            MeldableLinkedList<WeightedEdge> mst = OptimalMST.compute(verticesCount, contractedEdges);
            System.out.printf("OptimalMST Took %s ms\n", System.currentTimeMillis() - now);
            System.out.printf("Total weight: %s\n\n", mst.toList().stream().mapToLong(WeightedEdge::weight).sum());
        }

        contractedEdges = weightedEdges.stream().map(ContractedEdge::new).collect(Collectors.toList());
        MeldableLinkedList<ContractedEdge<WeightedEdge>> medldeableEdges = new MeldableLinkedList<>();
        contractedEdges.forEach(medldeableEdges::add);
        AdjacencyList adjacencyList = AdjacencyList.of(verticesCount, medldeableEdges);
        for (int i = 0; i < 1; i++) {

            now = System.currentTimeMillis();
            MeldableLinkedList<ContractedEdge<WeightedEdge>> mst2 = FredmanTarjanMST.findMst(verticesCount, adjacencyList);
            System.out.printf("FredmanTarjanMST Took %s ms\n", System.currentTimeMillis() - now);
            System.out.printf("Total weight: %s\n\n", mst2.toList().stream().mapToLong(DirectedEdge::weight).sum());
        }

        for (int i = 0; i < 1; i++) {
            contractedEdges = weightedEdges.stream().map(ContractedEdge::new).collect(Collectors.toList());
            now = System.currentTimeMillis();
            MeldableLinkedList<ContractedEdge<WeightedEdge>> mst3 = KruskalMST.computeContracted(verticesCount, contractedEdges);
            System.out.printf("KruskalMST Took %s ms\n", System.currentTimeMillis() - now);
            System.out.printf("Total weight: %s\n\n", mst3.toList().stream().mapToLong(DirectedEdge::weight).sum());
        }

    }
}