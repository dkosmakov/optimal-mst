package org.algorithm.mst.launcher;

import org.algorithm.mst.graph.edge.WeightedEdge;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GraphGenerator {

    private static StreamTokenizer streamTokenizer = new StreamTokenizer(
            new BufferedReader(new InputStreamReader(System.in)));

    private static final Random RANDOM = new Random();

    private static int nextInt() {
        try {
            streamTokenizer.nextToken();
        } catch (IOException e) {
            //
        }
        return (int) streamTokenizer.nval;
    }

    // Generates random connected graph with V vertices and E edges count
    // Be aware that it's random algorithm and if edges count is close to maximum it could take too much time
    public static void main(String[] args) throws IOException {
        int vertices = nextInt();
        int edges = nextInt();

        checkInput(vertices, edges);

        Set<WeightedEdge> edgeSet = generateRandomSpanningTree(vertices);
        generateRestOfEdges(vertices, edges, edgeSet);

        List<Integer> edgeWeights = edgeWeights(edges);

        writeGraphToFile(vertices, edgeSet, edgeWeights);
    }

    private static void generateRestOfEdges(int vertices, int edges, Set<WeightedEdge> edgeSet) {
        while (edgeSet.size() != edges) {
            int vertexFrom = RANDOM.nextInt(vertices);
            int vertexTo = RANDOM.nextInt(vertices);
            if (vertexFrom == vertexTo) {
                continue;
            }
            if (vertexFrom > vertexTo) {
                int temp = vertexFrom;
                vertexFrom = vertexTo;
                vertexTo = temp;
            }
            WeightedEdge newOne = new WeightedEdge(vertexFrom, vertexTo, 0L);
            edgeSet.add(newOne);
        }
    }

    private static void writeGraphToFile(int vertices, Set<WeightedEdge> edgeSet, List<Integer> edgeWeights) throws IOException {
        FileWriter fileWriter = new FileWriter("graph.txt");
        int i = 0;
        fileWriter.write(vertices + "\n");
        for (WeightedEdge edge: edgeSet) {
            fileWriter.write(edge.from() + " " + edge.to() + " " + edgeWeights.get(i));
            fileWriter.write('\n');
            i++;
        }
        fileWriter.close();
    }

    private static Set<WeightedEdge> generateRandomSpanningTree(int vertices) {
        List<Integer> allVerticesShuffled = IntStream.range(0, vertices).boxed().collect(Collectors.toList());
        List<Integer> verticesInSpanningTree = new ArrayList<>();
        Set<WeightedEdge> edgeList = new HashSet<>();

        Collections.shuffle(allVerticesShuffled);

        for (int i = 0; i < vertices; i++) {
            Integer toVertex = allVerticesShuffled.get(i);
            if (!verticesInSpanningTree.isEmpty()) {
                int randomPosition = RANDOM.nextInt(i);
                Integer randomFromVertex = verticesInSpanningTree.get(randomPosition);
                edgeList.add(randomFromVertex < toVertex ? new WeightedEdge(randomFromVertex, toVertex, 0L) : new WeightedEdge(toVertex, randomFromVertex, 0L));
            }
            verticesInSpanningTree.add(toVertex);
        }
        return edgeList;
    }

    private static List<Integer> edgeWeights(int edges) {
        return IntStream.range(0, edges).boxed().collect(Collectors.toList());
    }

    private static void checkInput(long vertices, long edges) {
        if (edges < vertices - 1 || edges > vertices * (vertices - 1) / 2) {
            throw new IllegalArgumentException("In connected graph edges count should be in range [" + (vertices - 1)
                    + ", " + vertices * (vertices - 1) / 2 + "]");
        }
    }

}
