package org.algorithm.mst.graph.edge;

import lombok.EqualsAndHashCode;

/**
 * 
 * Edges in a subgraph may need to be renamed. For example, imagine that you are contracting certain sets of vertices
 * of a graph. The new graph keeps all edges between the individual components, but the contracted edges link between different
 * vertices. This behavior is reflected in this class. <br>
 * There are multiple implementations of this class that do the same thing and could be used interchangeably.
 * The reason for this is to give the edge variables that are used throughout the mst algorithms a semantic value, so that
 * it is easier to understand what the purpose of a given edge in the algorithm is.
 */
@EqualsAndHashCode
abstract class AbstractRenamedEdge<T extends DirectedEdge<T>> implements DirectedEdge<AbstractRenamedEdge<T>> {

    protected final int from;
    protected final int to;
    public final T original;

    /**
     * Creates a new abstract renamed edge with new {@link from}, {@link to}, internally referencing the original edge.
     * @param from the new from
     * @param to the new to
     * @param original the edge that this new edge should reference
     */
    AbstractRenamedEdge(final int from, final int to, final T original) {
        this.from = from;
        this.to = to;
        this.original = original;
    }

    @Override
    public int from() {
        return from;
    }

    @Override
    public int to() {
        return to;
    }

    @Override
    public long weight() {
        return original.weight();
    }

    /**
     * Return a {@link String} representation of this edge.
     * @param name the name of this edge
     * @return a {@link String} representation of this edge
     */
    String representation(final String name) {
        return String.format("%s(%s, %s, original=%s)", name, from, to, original);
    }

    @Override
    public String toString() {
        return representation("AbstractRenamedEdge");
    }

    @Override
    public int compareTo(AbstractRenamedEdge<T> o) {
        return original.compareTo(o.original);
    }
}
