package org.algorithm.mst.graph.edge;

/**
 * 
 * Represents an Edge with generic weight type.
 */
public interface DirectedEdge<T extends DirectedEdge> extends Comparable<T> {
	/**
	 * Returns where the edge starts.
	 * @return where the edge starts
	 */
    int from();
    /**
     * Returns where the edge ends.
     * @return where the edge ends
     */
    int to();
    /**
     * Returns the weight of the edge.
     * @return the weight of the edge
     */
    long weight();
    /**
     * Returns a new edge with {@link #from} and {@link #to} swapped.
     * @return a new edge with {@link #from} and {@link #to} swapped
     */
    T reversed();

}
