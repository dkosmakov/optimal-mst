package org.algorithm.mst.graph.edge;

/**
 * 
 * An implementation of {@link AbstractRenamedEdge}
 * @param <T> the type of this edge
 */
public final class RenamedEdge<T extends DirectedEdge<T>> extends AbstractRenamedEdge<T> {

    public RenamedEdge(final int from, final int to, final T original) {
        super(from, to, original);
    }

    @Override
    public RenamedEdge<T> reversed() {
        return new RenamedEdge<>(to, from, original);
    }

    @Override
    public String toString() {
        return representation("RenamedEdge");
    }

}
