package org.algorithm.mst.graph.edge;

import lombok.Value;

import static java.util.Comparator.comparingInt;
import static java.util.Comparator.comparingLong;

@Value
public final class WeightedEdge implements DirectedEdge<WeightedEdge> {

    private final int from;
    private final int to;
    private final Long weight;

    public WeightedEdge reweighted(long weight) {
        return new WeightedEdge(from, to, weight);
    }

    @Override
    public int from() {
        return from;
    }

    @Override
    public int to() {
        return to;
    }

    @Override
    public WeightedEdge reversed() {
        return new WeightedEdge(to, from, weight);
    }

    @Override
    public long weight() {
        return weight;
    }

    @Override
    public String toString() {
        return String.format("Edge(%s, %s, w=%s)", from, to, weight);
    }

    @Override
    public int compareTo(WeightedEdge other) {
        return comparingLong(WeightedEdge::weight)
                .thenComparingInt(WeightedEdge::from)
                .thenComparingInt(WeightedEdge::to)
                .compare(this, other);
    }
}
