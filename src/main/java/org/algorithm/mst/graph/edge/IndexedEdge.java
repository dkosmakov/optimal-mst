package org.algorithm.mst.graph.edge;

import lombok.Value;

/**
 * 
 * A special type of edge that is associated with an index. This is used in the creation of the optimal decision trees
 * @param <T> the edge type of the edge that this edge references
 */
@Value
public final class IndexedEdge<T extends DirectedEdge<T>> implements DirectedEdge<IndexedEdge<T>> {

	int index;
	T edge;

	@Override
	public int compareTo(IndexedEdge<T> o) {
		return edge.compareTo(o.edge);
	}

	@Override
	public int from() {
		return edge.from();
	}

	@Override
	public int to() {
		return edge.to();
	}

	@Override
	public long weight() {
		return edge.weight();
	}

	@Override
	public IndexedEdge<T> reversed() {
		return new IndexedEdge<>(index, edge.reversed());
	}

	@Override
	public String toString() {
		return String.format("IndexedEdge(index=%s, edge=%s)", index, edge);
	}
}

