package org.algorithm.mst.graph;


import org.algorithm.mst.graph.edge.DirectedEdge;
import org.algorithm.mst.util.MeldableLinkedList;

public class Graph<T extends DirectedEdge> {

    /**
     * The number of vertices in the graph
     */
    public final int vertices;
    /**
     * The {@link MeldableLinkedList<T>} of edges in the graph
     */
    public final MeldableLinkedList<T> edges;

    /**
     * Creates a new Graph with the given number of vertices and the {@link MeldableLinkedList}
     * @param vertices the number of vertices in the graph
     * @param edges the {@link MeldableLinkedList} of edges
     */
    public Graph(int vertices, MeldableLinkedList<T> edges) {
        this.vertices = vertices;
        this.edges = edges;
    }

    @Override
    public String toString() {
        return String.format("Graph(vertices=%s, edges=\n%s)", vertices, edges);
    }

}
