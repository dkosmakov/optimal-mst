package org.algorithm.mst;

import org.algorithm.mst.graph.edge.ContractedEdge;
import org.algorithm.mst.graph.edge.DirectedEdge;
import org.algorithm.mst.util.AdjacencyList;
import org.algorithm.mst.util.MeldableLinkedList;
import org.algorithm.mst.util.heap.keithschwarz.FibonacciHeap;

import java.util.*;

public final class FredmanTarjanMST {

    public static class Vertex<T extends DirectedEdge<T>> {

        int index;
        long weight = Long.MAX_VALUE;
        ContractedEdge<T> mstEdge = null;

        Vertex(int index) {
            this.index = index;
        }
    }

    public static <T extends DirectedEdge<T>> MeldableLinkedList<ContractedEdge<T>> findMst(int verticesCount, AdjacencyList adjacencyList) {

        return compute(verticesCount, adjacencyList);
    }

    public static <T extends DirectedEdge<T>> MeldableLinkedList<ContractedEdge<T>> compute(int verticesCount, AdjacencyList adjacencyList) {
        MeldableLinkedList<ContractedEdge<T>> result = new MeldableLinkedList<>();

        //init util structures
        Vertex<T>[] vertices = new Vertex[verticesCount];
        FibonacciHeap.Entry<Integer>[] indexToEntry = new FibonacciHeap.Entry[verticesCount];
        for (int i = 0; i < verticesCount; i++) {
            vertices[i] = new Vertex<>(i);
        }

        boolean[] inHeap = new boolean[verticesCount];
        boolean[] inMst = new boolean[verticesCount];

        FibonacciHeap<Integer> queue = new FibonacciHeap<>();
        //Start new tree root
        indexToEntry[0] = queue.enqueue(0, vertices[0].weight);

        while (!queue.isEmpty() && result.size() != verticesCount - 1) {

            Integer currentMin = queue.dequeueMin().getValue();
            if (vertices[currentMin].mstEdge != null) {
                result.add(vertices[currentMin].mstEdge);
            }
            inMst[vertices[currentMin].index] = true;
            addNeighborsToHeap(indexToEntry, inMst, inHeap, vertices, adjacencyList, queue, vertices[currentMin]);
        }

        return result;
    }

   /* public static <T extends DirectedEdge<T>> MeldableLinkedList<ContractedEdge<T>> computeRecursive(int verticesCount, MeldableLinkedList<ContractedEdge<T>> edges) {
        MeldableLinkedList<ContractedEdge<T>> result = new MeldableLinkedList<>();

        //init util structures
        Vertex<T>[] vertices = new Vertex[verticesCount];
        HashSet<Integer> nonMstVertices = new HashSet<>();
        for (int i = 0; i < verticesCount; i++) {
            vertices[i] = new Vertex<>(i);
            nonMstVertices.add(i);
        }

        AdjacencyList adjacencyList = AdjacencyList.of(verticesCount, edges);
        WeightedUnionFind treesDisjointSet = new WeightedUnionFind(verticesCount);
        FibonacciHeap<Vertex<T>> queue;
        boolean[] inHeap;

        int maxHeapSize = (int) Math.pow(2, 2 * edges.size() / verticesCount);
        //long maxHeapSize = 100000000;

        //Single Fredman-Tarjan iteration
        while (!nonMstVertices.isEmpty()) {

            queue = new FibonacciHeap2<>();
            inHeap = new boolean[verticesCount];

            //Start new tree root
            Vertex<T> currentMin = vertices[nextVertexId(nonMstVertices)];
            nonMstVertices.remove(currentMin.index);
            int treeRoot = treesDisjointSet.root(currentMin.index);
            addNeighborsToHeap(treesDisjointSet, inHeap, vertices, adjacencyList, queue, currentMin);

            while (!queue.empty() && (queue.size() < maxHeapSize)) {

                currentMin = queue.pop();
                nonMstVertices.remove(currentMin.index);
                result.add(currentMin.mstEdge);

                int currentRoot = treesDisjointSet.root(currentMin.index);
                treesDisjointSet.connect(currentMin.index, treeRoot);

                if (currentRoot == currentMin.index) {
                    addNeighborsToHeap(treesDisjointSet, inHeap, vertices, adjacencyList, queue, currentMin);
                } else {
                    //Connection to other tree found. Merging
                    break;
                }
            }
        }

        List<List<Integer>> components = treesDisjointSet.getComponents();

        //Contraction + Recursion
        if (components.size() > 1) {
            MeldableLinkedList<ContractedEdge<T>> contractedEdges = GraphUtils.contract(verticesCount, components, edges);
            MeldableLinkedList<ContractedEdge<T>> componentMst = compute(components.size(), contractedEdges);
            MeldableLinkedList<ContractedEdge<T>> flatten = GraphUtils.extractOriginal(componentMst);

            result.meld(flatten);
        }

        return result;
    }*/

    private static <T extends DirectedEdge<T>> void addNeighborsToHeap(FibonacciHeap.Entry<Integer>[] indexToEntry, boolean[] inMst, boolean[] inHeap, Vertex<T>[] vertices, AdjacencyList<ContractedEdge<T>> adjacencyList, FibonacciHeap<Integer> queue, Vertex<T> currentMin) {
        for (ContractedEdge<T> e : adjacencyList.get(currentMin.index)) {
            if (!inMst[e.to()]) {
                updateHeap(indexToEntry, inHeap, vertices, queue, e);
            }
        }
    }

    /*private static <T extends DirectedEdge<T>> void addNeighborsToHeap(WeightedUnionFind treesDisjointSet, boolean[] inHeap, Vertex<T>[] vertices, AdjacencyList<ContractedEdge<T>> adjacencyList, FibonacciHeap<Vertex<T>> queue, Vertex<T> currentMin) {
        for (ContractedEdge<T> e : adjacencyList.get(currentMin.index)) {
            if (!treesDisjointSet.connected(e.to(), currentMin.index)) {
                updateHeap(inHeap, vertices, queue, e);
            }
        }
    }*/

    private static <T extends DirectedEdge<T>> void updateHeap(FibonacciHeap.Entry<Integer>[] indexToEntry, boolean[] inHeap, Vertex<T>[] vertices, FibonacciHeap<Integer> queue, ContractedEdge<T> e) {
        int to = e.to();
        if (inHeap[to]) {
            if (vertices[to].weight > e.weight()) {
                vertices[to].weight = e.weight();
                vertices[to].mstEdge = e;
                queue.decreaseKey(indexToEntry[to], vertices[to].weight);
            }
        } else {
            vertices[to].weight = e.weight();
            vertices[to].mstEdge = e;
            indexToEntry[to] = queue.enqueue(to, vertices[to].weight);
            inHeap[to] = true;
        }
    }

    private static Integer nextVertexId(HashSet<Integer> nonMstVertices) {
        return nonMstVertices.iterator().next();
    }
}
